# pe/vim

most importantly, supports [parinfer](https://shaunlebron.github.io/parinfer/)
for easy scheming.

## install

```
mkdir ~/.vim && cd ~/.vim
fossil open https://heropunch.io/xjix/pe-vim
[sudo] python3 -m pip install isort[requirements,pipfile]
```

if you have vim open somewhere you can `:so ~/.vim/vimrc` to load the new config
or restart your computer.

### update

```
cd ~/.vim && fossil pull
```

## hints

```vim
" expand the fucking hint thing
:ALEDetail
```

## license

check [`pack/pe/start`](/dir?ci=tip&name=pack/pe/start) for third party
scripts and their license terms (bsd/mit/public domain). `pe/vim` scripts long
enough to copyright are [chaotic software](http://xj-ix.luxe/wiki/chaotic-software/)

