" colorscheme solarized
set background=dark

" tabs > spaces
" https://archive.is/yD2Zb
set tabstop=2
set shiftwidth=2
set noet

" show invisible characters
set list
set listchars=eol:⏎,tab:··,trail:⎵,nbsp:⎵

" -- use ctrl-[hjkl] to select the active split
nmap <silent> <c-h> :wincmd h<CR>
nmap <silent> <c-j> :wincmd j<CR>
nmap <silent> <c-k> :wincmd k<CR>
nmap <silent> <c-l> :cincmd l<CR>

" -- misc options
filetype on
filetype plugin indent on
syntax on
set autoindent
set number
set ruler
set linebreak
set encoding=utf-8
set laststatus=2
set modeline
set modelines=5
set textwidth=80
set backspace=indent,eol,start
set hid

" -- parinfer options
let g:vim_parinfer_globs = ['*.scm', '*.egg']
"let g:vim_parinfer_filetypes: ['lisp']
let g:vim_parinfer_mode = 'indent'
