setlocal shiftwidth=2
setlocal tabstop=2
setlocal expandtab
highlight OverLength ctermbg=red ctermfg=white
call matchadd('Overlength', '\%121v.\+', 100)
