" word wrap!
" https://vim.fandom.com/wiki/Word_wrap_without_line_breaks
set wrap
set linebreak
set textwidth=0
set wrapmargin=0
